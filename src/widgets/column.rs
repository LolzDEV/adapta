use crate::utils::{Position, Alignment, px_to_wunit, SizeType};
use super::Widget;
/// A column creates a vertical array of children.
/// For horizontal variant, see [crate::widgets::column::Column].
/// 
/// This sample uses a [Column] to arrange 
/// three [crate::widgets::Label] widgets vertically.
/// 
/// ```
///   use adapta::widgets::column::Column;
///   use adapta::utils::Text;   
///   use adapta::utils::Color;
///   use adapta::utils::TextStyle;
///   use adapta::widgets::Label;
///   Column::new().set_children( 
///      vec![
///         Box::new(Label::new().with_text(Text {
///             text: "1".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///          }
///      })),
///         Box::new(Label::new().with_text(Text {
///             text: "2".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///          }
///
///         Box::new(Label::new().with_text(Text {
///      })),
///             text: "3".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///           }
///      })),
///    ]
///  );
/// ```
/// 
/// You can also set a vertical or horizontal alignment.
/// In the following sample, the [horizontal_alignment] and [vertical_alignment] are 
/// set to [Alignment::Center] so that the children are center-aligned. 
/// ```
/// use adapta::widgets::column::Column;
/// use adapta::utils::Alignment;
/// Column::new().horizontal_alignment(Alignment::Center).vertical_alignment(Alignment::Center);
/// ```
/// 
/// See also: 
/// 
/// * [crate::widgets::row::Row] for a horizontal equivalent.
/// 
/// 
pub struct Column {
    pub children: Vec<Box<dyn Widget>>,
    vertical_alignment: Alignment,
    horizontal_alignment: Alignment,
    size: (f32, f32),
    screen_size: (f32, f32),
    parent_size: (f32, f32),
    position: Position,
    size_type: SizeType,
}

impl Column {
    /// Creates a new Column.
    /// The [vertical_alignment] is set to [Alignment::Start] 
    /// and the [horizontal_alignment] is set to [Alignment::Center] by default.
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
            vertical_alignment: Alignment::Start,
            horizontal_alignment: Alignment::Center,
            size: (0., 0.),
            position: Position { x: 0., y: 0. },
            screen_size: (0., 0.),
            parent_size: (0., 0.),
            size_type: SizeType::Fill
        }
    }
    /// Sets the children widgets of this [Column].
    /// You must wrap each one with a [Box].
    /// 
    /// # Example
    /// ```
    /// let column = Column::new().set_children(
    ///     vec! [
    ///         Box::new(Label::new()),
    ///         Box::new(Label::new()),
    ///     ],
    /// );
    /// assert_eq!(column.children.len(), 1);
    /// ```
    pub fn set_children(mut self, childrens: Vec<Box<dyn Widget>>) -> Self {
        self.children.extend(childrens);
        self
    }
 
    /// Appends only a child widget to this [Column].
    /// 
    /// # Examples
    /// 
    /// ``` 
    /// let column = Column::new().child(Box(Label::new()));
    /// ```
    /// The example above creates a column and appends a Label widget.
    pub fn child(mut self, child: Box<dyn Widget>) -> Self {
        self.children.push(child);
        self
    }
    /// Sets the vertical alignment of this [Column].
    /// Determines how the children should be placed along the vertical axis.
    /// 
    /// # Example
    /// 
    /// ```
    /// use adopta::utils::Alignment;
    /// let column = Column::new().vertical_alignment(Alignment::Center);
    /// ```
    pub fn vertical_alignment(mut self, alignment: Alignment) -> Self {
        self.vertical_alignment = alignment;
        self
    }
    /// Sets the horizontal alignment of this [Column].
    /// Determines how the children should be placed along the horizontal axis.
    /// 
    /// Basic usage:
    /// ```
    /// use adopta::utils::Alignment;
    /// let column = Column::new().horizontal_alignment(Alignment::Start);
    /// ```
    pub fn horizontal_alignment(mut self, alignment: Alignment) -> Self {
        self.horizontal_alignment = alignment;
        self
    }
    
    pub fn size_type(mut self, size_type: SizeType) -> Self {
        self.size_type = size_type;
        self
    }
}

impl Widget for Column {
    fn draw(&self, renderer: &mut Box<dyn crate::renderer::Renderer>) {
        for child in self.children.iter() {
            child.draw(renderer);
        }
    }

    fn set_size(&mut self, size: (f32, f32)) {
        
        if let SizeType::Content = self.size_type {
            let mut max_width = 0.;
            for child in self.children.iter() {
                if child.get_size().0 > max_width { max_width = child.get_size().0 }
            }

            let mut height = 0.;
            for child in self.children.iter() {
                height += child.get_size().1;
            }

            self.size = (max_width, height);
        } else if let SizeType::Fill = self.size_type {
            self.size = size;
        }

        let w_height = size.1 / self.children.len() as f32;

        for child in self.children.iter_mut() {
            child.set_parent_size(size);
            child.set_size((size.0, w_height));
        }
    }

    fn get_size(&self) -> (f32, f32) {
        self.size
    }

    fn set_position(&mut self, position: Position) {
        self.position = position;
        let mut max_width = 0.;
        for child in self.children.iter() {
            if child.get_size().0 > max_width { max_width = child.get_size().0 }
        }

        let mut position = position;

        match self.horizontal_alignment {
            Alignment::Center => {
                position = Position { x: (px_to_wunit(self.parent_size.0 as u32, self.parent_size.0 as u32) / 2.) - (max_width / 2.), y: position.y}
            },
            Alignment::Start => (), // Fixes self-assignment of `position` to `position`.
            Alignment::End => position = Position { x: self.position.x + self.size.0 - max_width, y: position.y },
        }
        let mut current_pos = position;
        for child in self.children.iter_mut() {
            child.set_position(current_pos);
            current_pos = Position { x: current_pos.x, y: current_pos.y + child.get_size().1 };
        }
    }

    fn get_position(&self) -> Position {
        self.position
    }

    fn init(&mut self, renderer: &mut Box<dyn crate::renderer::Renderer>) {
        for child in self.children.iter_mut() {
            child.init(renderer);
        }
    }

    fn set_screen_size(&mut self, screen_size: (f32, f32)) {
        self.screen_size = screen_size;

        for child in self.children.iter_mut() {
            child.set_screen_size(screen_size);
        }
    }

    fn set_parent_size(&mut self, size: (f32, f32)) {
        self.parent_size = size;
    }

    fn click(&mut self, pos: (f32, f32)) {
        //TODO: Improve algorithm
        for child in self.children.iter_mut() {
            if pos.0 >= child.get_position().x && pos.0 <= child.get_position().x + child.get_size().0 {
                if pos.1 >= child.get_position().y && pos.1 <= child.get_position().y + child.get_size().1 {
                   child.click(pos);
                   return;
                }
            }
        }
    }
}
