use std::{cell::RefCell, rc::Rc};

use winit::{
    dpi::PhysicalPosition,
    event::{ElementState, KeyboardInput, MouseButton, VirtualKeyCode},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use crate::{
    renderer::{wgpu_renderer::WgpuRenderer, Renderer},
    utils::{px_to_hunit, px_to_wunit, Position},
    widgets::Widget,
};

#[derive(Debug, Clone, Copy, Default)]
pub struct ApplicationInfo<'a> {
    pub name: &'a str,
    pub id: &'a str,
    pub version: &'a str,
}

pub struct Application<'a> {
    info: ApplicationInfo<'a>,
    main_window: winit::window::Window,
    event_loop: winit::event_loop::EventLoop<()>,
    renderer: Box<dyn Renderer>,
}

impl<'a> Application<'a> {
    pub fn new(info: ApplicationInfo<'a>) -> Self {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_title(info.name)
            .build(&event_loop)
            .unwrap();
        let renderer = Box::new(WgpuRenderer::new(&window));

        Self {
            info,
            main_window: window,
            event_loop,
            renderer,
        }
    }

    /// This method changes the current rendering backend, by default `WgpuRenderer` is used
    pub fn set_renderer(&mut self, renderer: Box<dyn Renderer>) {
        self.renderer = renderer;
    }

    pub fn get_info(&self) -> ApplicationInfo<'a> {
        self.info
    }
}

// After the applicaiton is created, use this function to run it passing the main widget
pub fn run_app(mut app: Application, mut root: Box<dyn Widget>) {
    let mut mouse_pos = PhysicalPosition::new(0., 0.);
    let window = app.main_window;

    let (width, height) = (window.inner_size().width, window.inner_size().height);

    root.set_size((px_to_wunit(width, width), px_to_hunit(height, height)));
    root.set_screen_size((width as f32, height as f32));
    root.init(&mut app.renderer);

    root.set_position(Position { x: 0., y: 0. });

    let root = Rc::new(RefCell::new(root));

    app.renderer.draw_widget(root.clone());

    app.event_loop.run(move |e, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match e {
            winit::event::Event::WindowEvent { window_id, event } => {
                if window_id == window.id() {
                    match event {
                        winit::event::WindowEvent::Resized(new_size) => {
                            app.renderer.resize(new_size);
                            root.borrow_mut().set_size((
                                px_to_wunit(new_size.width, new_size.width),
                                px_to_hunit(new_size.height, new_size.height),
                            ));
                            root.borrow_mut()
                                .set_screen_size((new_size.width as f32, new_size.height as f32));
                            root.borrow_mut().init(&mut app.renderer);
                            root.borrow_mut()
                                .set_parent_size((new_size.width as f32, new_size.height as f32));
                            root.borrow_mut().set_position(Position { x: 0., y: 0. });
                        }
                        winit::event::WindowEvent::CloseRequested => {
                            *control_flow = ControlFlow::Exit
                        }
                        winit::event::WindowEvent::KeyboardInput { input, .. } => {
                            if let KeyboardInput {
                                virtual_keycode: Some(code),
                                ..
                            } = input
                            {
                                match code {
                                    VirtualKeyCode::Escape => *control_flow = ControlFlow::Exit,
                                    _ => (),
                                }
                            }
                        }
                        winit::event::WindowEvent::CursorMoved { position, .. } => {
                            mouse_pos = position;

                            root.borrow_mut().mouse_move((
                                px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width),
                                px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height)
                            ));

                            if px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width) >= root.borrow().get_position().x && px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width) as f32 <= root.borrow().get_position().x + root.borrow().get_size().0 {
                                if px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height) as f32 >= root.borrow().get_position().y && px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height) <= root.borrow().get_position().y + root.borrow().get_size().1 {
                                    root.borrow_mut().hover((
                                        px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width),
                                        px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height)
                                    ));
                                }
                            }
                        }
                        winit::event::WindowEvent::MouseInput {
                            state: ElementState::Pressed,
                            button,
                            ..
                        } => match button {
                            MouseButton::Left => {
                                println!("{:?}", mouse_pos);
                                if px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width) >= root.borrow().get_position().x && px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width) <= root.borrow().get_position().x + root.borrow().get_size().0 {
                                    if px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height) >= root.borrow().get_position().y && px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height) <= root.borrow().get_position().y + root.borrow().get_size().1 {
                                        root.borrow_mut().click((
                                            px_to_wunit(mouse_pos.x as u32, app.renderer.get_size().width),
                                            px_to_hunit(mouse_pos.y as u32, app.renderer.get_size().height)
                                        ));
                                    }
                                }
                                
                            },
                            MouseButton::Right => todo!(),
                            MouseButton::Middle => todo!(),
                            MouseButton::Other(_) => todo!(),
                        },
                        _ => (),
                    }
                }
            }
            winit::event::Event::MainEventsCleared => window.request_redraw(),
            winit::event::Event::RedrawRequested(_) => {
                root.borrow().draw(&mut app.renderer);
                if let Err(e) = app.renderer.render() {
                    match e {
                        wgpu::SurfaceError::Lost => app.renderer.resize(app.renderer.get_size()),
                        wgpu::SurfaceError::OutOfMemory => *control_flow = ControlFlow::Exit,
                        _ => (),
                    }
                }
            }
            _ => (),
        }
    });
}
