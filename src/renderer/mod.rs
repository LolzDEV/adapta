use std::{rc::Rc, cell::RefCell};

use cgmath::{Matrix4, Vector3, SquareMatrix};
use wgpu::SurfaceError;
use winit::dpi::PhysicalSize;

use crate::{widgets::{Widget, text::Text}, utils::{Color, Position}};

pub mod wgpu_renderer;

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vertex {
    position: [f32; 2],
    color: [f32; 3],
}

impl Vertex {
    pub fn new(x: f32, y: f32, color: Color) -> Self {
        Self {
            position: [x, y],
            color: [color.r, color.g, color.b]
        }
    }

    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x2,
                },
                wgpu::VertexAttribute {
                    offset: std::mem::size_of::<[f32; 2]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x3,
                }
            ]
        }
    }
}

/// Trait for implementing rendering backends
pub trait Renderer {
    fn render(&mut self) -> Result<(), SurfaceError>;
    fn resize(&mut self, new_size: PhysicalSize<u32>);
    fn draw_widget(&mut self, widget: Rc<RefCell<Box<dyn Widget>>>);
    fn draw_text(&mut self, text: Text, position: Position);
    fn get_size(&self) -> PhysicalSize<u32>;
    fn get_device(&self) -> &wgpu::Device;
    fn get_transform_layout(&self) -> &wgpu::BindGroupLayout;
    fn get_text_size(&mut self, text: Text) -> (f32, f32);
}

#[derive(Debug, Clone, Copy)]
pub struct Transform {
    transformation: Matrix4<f32>
}

impl Transform {
    pub fn new(position: Vector3<f32>) -> Self {
        let transformation = Matrix4::from_translation(position);

        Self {
            transformation
        }
    }

    pub fn translate(&mut self, translation: Vector3<f32>) {
        self.transformation += Matrix4::from_translation(translation);
    }

    pub fn scale(&mut self, scale: f32) {
        self.transformation += Matrix4::from_scale(scale);
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, bytemuck::Pod, bytemuck::Zeroable)]
pub struct TransformUniform {
    transformation: [[f32; 4]; 4]
}

impl TransformUniform {
    pub fn new() -> Self {
        Self {
            transformation: Matrix4::identity().into()
        }
    }

    pub fn update_transformation(&mut self, transform: Transform) {
        self.transformation = transform.transformation.into();
    }
}

#[derive(Debug, Clone, Copy)]
pub struct OrthographicProjection {
    proj: Matrix4<f32>
}

impl OrthographicProjection {
    pub fn new(width: f32, height: f32) -> Self{
        Self { proj: cgmath::ortho(0., width, height, 0., 1., -1.) }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, bytemuck::Pod, bytemuck::Zeroable)]
pub struct OrthographicProjectionUniform {
    proj: [[f32; 4]; 4]
}

impl OrthographicProjectionUniform {
    pub fn new() -> Self {
        Self {
            proj: Matrix4::identity().into()
        }
    }

    pub fn update_projection(&mut self, projection: OrthographicProjection) {
        self.proj = projection.proj.into();
    }
}