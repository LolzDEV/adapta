use adapta::{
    application::{run_app, Application, ApplicationInfo},
    utils::Color,
    widgets::{Label, text::{Text, TextStyle}, column::Column},
};

fn main() {
    let app = Application::new(ApplicationInfo {
        name: "Example Application",
        id: "com.lolzdev.ExampleApp",
        version: "1.0",
    });
    
    let column = Column::new().set_children(
        vec! [
            Box::new(Label::new(Text::with_style(String::from("Label constructors"), TextStyle::default_scaled(24.0)))),
            Box::new(Label::new(Text::with_style(String::from("Label constructors"), TextStyle::new(24.0, Color::new_rgb(1.0, 0.0, 0.0))))),
        ]
    );
    run_app(
        app,
        Box::new(column)
    );
}
