# Adapta 
![Lines of code](https://img.shields.io/tokei/lines/gitlab/LolzDEV/adapta?label=lines%20of%20code) ![Repo status](https://img.shields.io/badge/repo%20status-WIP-yellow)

Adapta is a modern UI framework, works on every device and it's simple to use.

## Why Adapta?
It uses WGPU for rendering so that every platform can be targeted without much changes to the actual codebase. Unlike common frameworks Adapta is meant to be as simple as possible so that everyone can learn how to use it just by looking at examples. **NOTE: This project is currently under development and the available features are not enough for a production usage. See below for a list of current features.**

## Features
- [X] Desktop (Linux, MacOS, Windows)
- [X] Web (WebGPU)
- [ ] Mobile (Android, iOS)
- [ ] Multi sampling
- [ ] Full set of default widgets
    - [X] Label
    - [ ] Text field
    - [X] Column
    - [X] Row
    - [ ] Text editor
    - [ ] Button
    - [ ] Icon
    - [ ] Icon button
- [X] Custom widgets

