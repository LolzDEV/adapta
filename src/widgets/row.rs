use crate::utils::{Position, Alignment, SizeType};

use super::Widget;


/// A Row creates a horizontal array of children.
/// For vertical variant, see [crate::widgets::column::Column]
/// 
/// The following example uses a [Row] to arrange 
/// three [crate::widgets::Label] widgets horizontally.
/// 
/// ```
///   use adapta::widgets::row::Row;
///   use adapta::utils::Text;   
///   use adapta::utils::Color;
///   use adapta::utils::TextStyle;
///   use adapta::widgets::Label;
///   Row::new().set_children( 
///      vec![
///         Box::new(Label::new().with_text(Text {
///             text: "1".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///          },
///      })),
///         Box::new(Label::new().with_text(Text {
///             text: "2".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///          },
///      })),
///         Box::new(Label::new().with_text(Text {
///             text: "3".to_string(),
///             style: TextStyle { scale: 18.0, color: Color::new_rgb(0., 0., 1.) 
///           },
///      })),
///    ]
///  );
/// ```
/// 
/// You can also set a vertical or horizontal alignment.
/// In the following sample, the [horizontal_alignment] and [vertical_alignment] are 
/// set to [Alignment::Center] so that the children are center-aligned. 
/// 
/// ```
/// use adapta::widgets::row::Row;
/// use adapta::utils::Alignment;
/// Row::new().horizontal_alignment(Alignment::Center).vertical_alignment(Alignment::Center);
/// ```
/// 
/// See also: 
/// 
/// * [crate::widgets::column::Column] for a vertical equivalent.
/// 
/// 
pub struct Row {
    pub children: Vec<Box<dyn Widget>>,
    vertical_alignment: Alignment,
    horizontal_alignment: Alignment,
    size: (f32, f32),
    screen_size: (f32, f32),
    parent_size: (f32, f32),
    position: Position,
    size_type: SizeType,
}

impl Row {
    /// Creates a new Row.
    /// The [vertical_alignment] is set to [Alignment::Start] 
    /// and the [horizontal_alignment] is set to [Alignment::Start] by default.
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
            vertical_alignment: Alignment::Start,
            horizontal_alignment: Alignment::Start,
            size: (0., 0.),
            position: Position { x: 0., y: 0. },
            screen_size: (0., 0.),
            parent_size: (0., 0.),
            size_type: SizeType::Fill
        }
    }
    
    
    /// Sets the children widgets of this [Row].
    /// You must wrap each one with a [Box].
    /// 
    /// # Example
    /// ```
    /// let row = Row::new().set_children(
    ///     vec! [
    ///         Box::new(Label::new()),
    ///         Box::new(Label::new()),
    ///     ],
    /// );
    /// assert_eq!(row.children.len(), 1);
    /// ```
    pub fn set_children(mut self, children: Vec<Box<dyn Widget>>) -> Self {
        self.children.extend(children);
        self
    }

    /// Appends only a child widget to this [Row].
    /// 
    /// # Examples
    /// 
    /// ``` 
    /// let row = Row::new().child(Box(Label::new()));
    /// ```
    /// The example above creates a row and appends a Label widget.
    pub fn child(mut self, child: Box<dyn Widget>) -> Self {
        self.children.push(child);
        self
    }
   /// Sets the vertical alignment of this [Row].
    /// Determines how the children should be placed along the vertical axis.
    /// 
    /// # Example
    /// 
    /// ```
    /// use adopta::utils::Alignment;
    /// let row = Row::new().vertical_alignment(Alignment::Center);
    /// ```
    pub fn vertical_alignment(mut self, alignment: Alignment) -> Self {
        self.vertical_alignment = alignment;
        self
    }
    /// Sets the horizontal alignment of this [Row].
    /// Determines how the children should be placed along the horizontal axis.
    /// 
    /// Basic usage:
    /// ```
    /// use adopta::utils::Alignment;
    /// let row = Row::new().horizontal_alignment(Alignment::Start);
    /// ```
    pub fn horizontal_alignment(mut self, alignment: Alignment) -> Self {
        self.horizontal_alignment = alignment;
        self
    }

    pub fn size_type(mut self, size_type: SizeType) -> Self {
        self.size_type = size_type;
        self
    }
}

impl Widget for Row {
    fn draw(&self, renderer: &mut Box<dyn crate::renderer::Renderer>) {
        for child in self.children.iter() {
            child.draw(renderer);
        }
    }

    fn set_size(&mut self, size: (f32, f32)) {
        if let SizeType::Content = self.size_type {
            let mut max_height = 0.;
            for child in self.children.iter() {
                if child.get_size().1 > max_height { max_height = child.get_size().1 }
            }

            let mut width = 0.;
            for child in self.children.iter() {
                width += child.get_size().0;
            }

            self.size = (width, max_height);
        } else if let SizeType::Fill = self.size_type {
            self.size = size;
        }

        let w_width = size.0 / self.children.len() as f32;

        for child in self.children.iter_mut() {
            child.set_parent_size(size);
            child.set_size((w_width, size.1));
        }
    }

    fn get_size(&self) -> (f32, f32) {
        self.size
    }

    fn set_position(&mut self, position: Position) {
        self.position = position;

        let mut max_height = 0.;
        for child in self.children.iter() {
            if child.get_size().1 > max_height { max_height = child.get_size().1 }
        }

        let mut position = position;

        match self.horizontal_alignment {
            Alignment::Center => {
                todo!()
            },
            Alignment::Start => (), // Fixes self-assignment of `position` to `position`
            Alignment::End => todo!(),
        }
        let mut current_pos = position;
        for child in self.children.iter_mut() {
            child.set_position(current_pos);
            current_pos = Position { x: current_pos.x + child.get_size().0, y: current_pos.y};
        }
    }

    fn get_position(&self) -> Position {
        self.position
    }

    fn init(&mut self, renderer: &mut Box<dyn crate::renderer::Renderer>) {
        for child in self.children.iter_mut() {
            child.init(renderer);
        }
    }

    fn set_screen_size(&mut self, screen_size: (f32, f32)) {
        self.screen_size = screen_size;

        for child in self.children.iter_mut() {
            child.set_screen_size(screen_size);
        }
    }

    fn set_parent_size(&mut self, size: (f32, f32)) {
        self.parent_size = size;
    }
}
