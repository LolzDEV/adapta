use std::{error::Error};

use ab_glyph::FontArc;

pub fn get_font() -> Result<ab_glyph::FontArc, Box<dyn Error>> {
    Ok(FontArc::try_from_slice(include_bytes!("../../assets/fonts/Roboto-Black.ttf")).unwrap())
}